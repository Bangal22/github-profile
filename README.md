# GitHub-Profile 

This project is made to find users' github profiles and view their statistics and repos.

![Github profile](/static/user-profile.jpg)


## Technologies 

This project is built with:
1. [SvelteKit](https://kit.svelte.dev/)
2. [TypeScript](https://www.typescriptlang.org/)
3. [Chart.js](https://www.chartjs.org/)
4. [Bootstrap](https://getbootstrap.com/)
5. [Vite](https://vitejs.dev/)

## Getting Started
### Install dependencies 
```
npm install
```


## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```