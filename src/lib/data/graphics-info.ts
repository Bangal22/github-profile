export const graphics = [
    {
        title: 'TOP 5 REPOSITORIES',
        values: 'stars',
        type: 'pie',
        graphicTitle: 'Top 5 repositories',
        top: true,
        bg: 'white'

    },
    {
        title: 'TOP LANGUAGES',
        values: 'languages',
        type: 'bar',
        graphicTitle: 'Top languages',
        top: false,
        bg: 'black'

    },
    {
        title: 'TOP 5 FORKS',
        values: 'forks',
        type: 'doughnut',
        graphicTitle: 'Top 5 forks',
        top: true,
        bg: 'white',

    },
];