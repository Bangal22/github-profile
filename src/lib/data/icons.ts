import type { Icons } from "../interface";

//In the future, I will join this file with lang-color.ts
export const icons : Icons[] = [
    {
        name : 'HTML', 
        icon: '<i class="devicon-html5-plain colored"></i>', 
    },
    {
        name: "CSS", 
        icon: '<i class="devicon-css3-plain colored"></i>', 
    }, 
    {
        name: "JAVASCRIPT", 
        icon: '<i class="devicon-javascript-plain colored"></i>', 

    },
    {
        name: "SVELTE", 
        icon: '<i class="devicon-svelte-plain colored"></i>', 

    },
    {
        name: "MYSQL", 
        icon: '<i class="devicon-mysql-plain colored"></i>', 

    },
    {
        name: "GITLAB", 
        icon: '<i class="devicon-gitlab-plain colored"></i>', 

    },
    {
        name: "PHP", 
        icon: '<i class="devicon-php-plain colored"></i>', 
    }
]