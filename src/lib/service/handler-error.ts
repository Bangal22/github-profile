import type { Body, Header } from "../interface";
export const handlerError = (response: Response, resource: string) => {
    const {headers, status} = response;
    const body : Body = {
        name : resource,
        errorType : status,
    }
    const header : Header =  {
        limit : headers.get('x-ratelimit-limit'), 
        remaining : headers.get('x-ratelimit-remaining'),
        time : headers.get('x-ratelimit-reset'),
    }

    if (status === 404) return { body, header, status : 0 }

    if (status === 200) return { res : response.json(), body, header, status: 1}

    return { body, header, status: 0}
}