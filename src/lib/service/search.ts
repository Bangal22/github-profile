import { handlerError } from "./handler-error";
import { environment } from "../environment";
import type { PetitionRes, RepoInfo, UserInfo } from "../interface";



export const reqGetRepos = async (resource: string) : Promise<PetitionRes> => {
    const response = await fetch(`${environment.api_url}/users/${resource}/repos?per_page=100`);
    const verification = handlerError(response, resource);
    const {status, body, header} = verification;
    
    if (!status) {
        return {
            body, 
            header, 
            response : [], 
            status
        }
    };
    
    const {res} = verification;
    const data = await res;
    const repo : [RepoInfo] = data.map(({ language, stargazers_count, size, forks_count, html_url, description, name }: RepoInfo) => {
        return {
            language,
            stargazers_count,
            size,
            forks_count,
            html_url,
            description,
            name
        }
    })

    return { body, header, response : repo, status }
}

export const reqGetUsers = async (resource: string) : Promise<PetitionRes> => {
    const response = await fetch(`${environment.api_url}/users/${resource}`);
    const verification = handlerError(response, resource);
    const {status, body, header} = verification;

    if (!status) {
        return {
            body, 
            header, 
            response : {}, 
            status,
        }
    };

    const {res} = verification;
    const data = await res;
    const {
        avatar_url,
        name,
        html_url,
        login,
        company,
        location,
        created_at,
        public_repos,
        followers,
        following
    }: UserInfo = data 

    return {
        body,
        header,
        response: {
            avatar_url,
            name,
            html_url,
            login,
            company,
            location,
            created_at,
            public_repos,
            followers,
            following
        },
        status
    }
}


