export interface UserInfo {
    avatar_url: string,
    name: string,
    html_url: string,
    login: string,
    company?: string,
    location?: string,
    created_at: Date,
    public_repos?: string,
    followers: number,
    following: number,
}