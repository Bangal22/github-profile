export interface RepoInfo {
    size: number,
    forks_count: number,
    html_url: string,
    description: string,
    stargazers_count: number,
    language?: string,
    name: string
}