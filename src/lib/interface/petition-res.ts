import type { Header, Body } from ".";

export interface PetitionRes {
    body: Body,
    header: Header, 
    response? : any, 
    status : number
}
