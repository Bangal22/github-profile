export interface Header {
    limit: string | null,
    remaining: string | null,
    time : string | null,
}