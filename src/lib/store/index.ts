import type { RepoInfo } from "../interface";
import { writable, type Writable } from "svelte/store";

export const user = writable();
export const repositories : Writable<[RepoInfo]> = writable();

