export const toNormalDate = (date : number | Date) => {
    return new Date(date).toLocaleDateString();
}
export const toNormalTime = ( date : number | Date) => {
    return new Date(date).toLocaleTimeString();
}